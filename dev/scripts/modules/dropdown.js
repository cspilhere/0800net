var dropdown = function() {
  try {
    let dropdown = document.querySelector('.js-dropdown');
    let dropdownTrigger = document.querySelector('.js-dropdown .dropdown__trigger');
    let dropdownContainer = document.querySelector('.js-dropdown .dropdown__container');
    let timer;

    dropdownTrigger.addEventListener('click', function() {
      clearTimeout(timer);
      dropdown.classList.toggle('is-open');
    });
    dropdownTrigger.addEventListener('mouseover', function(e) {
      clearTimeout(timer);
    });
    dropdownTrigger.addEventListener('mouseout', function(e) {
      clearTimeout(timer);
      dropdown.classList.remove('is-open');
    });

    dropdownContainer.addEventListener('mouseover', function(e) {
      clearTimeout(timer);
      dropdown.classList.add('is-open');
    });
    dropdownContainer.addEventListener('mouseout', function(e) {
      timer = setTimeout(function() {
        clearTimeout(timer);
        dropdown.classList.remove('is-open');
      }, 100);
    });
  } catch (e) {
    console.log('jsModule: dropdown | ', e);
  }
};

export default dropdown;
