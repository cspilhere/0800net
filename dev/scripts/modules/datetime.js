let foreach = Array.prototype.forEach;

var datetime = function() {

    var Pikaday = require('../plugins/pikaday');

    try {
      var picker = new Pikaday({
        field: document.querySelector('.js-date'),
        trigger: document.querySelector('.js-date-trigger'),
        format: 'D/MM/YYYY',
        i18n: {
          previousMonth : 'Mês anterior',
          nextMonth     : 'Próximo mês',
          months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
          weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
          weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
        }
      });

      var input = document.querySelector('.js-mask-hour');
      var mask = /^([0-2][0-3])([0-5][0-9])$/;
      var mask24 = /^([0-2][0-3]):([0-5][0-9])$/;
      var regex = /^[0-9]*$/;

      input.addEventListener('input', function(e) {
        if(!regex.test(e.target.value)) e.target.value = e.target.value.replace(/[a-zA-Z]/, '');
        if(mask.test(e.target.value)) e.target.value = e.target.value.replace(mask, '$1:$2');
        if(!mask24.test(e.target.value) && e.target.value.length > 4) e.target.value = '';
      });

      input.addEventListener('keypress', function(e) {
        if(!regex.test(e.target.value)) {
          e.preventDefault();
        }
      });

    } catch (e) {
      console.log('jsModule: datetime | ', e);
    }
};

export default datetime;
