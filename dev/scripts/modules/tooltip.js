let foreach = Array.prototype.forEach;

function tooltip() {
  try {
    let elementsWithTooltip = document.querySelectorAll('.js-tooltip');
    let tooltip = document.querySelector('.tooltip');
    let timer;

    foreach.call(elementsWithTooltip, function(item) {
      let tooltipText = item.getAttribute('title');
      item.removeAttribute('title');

      item.addEventListener('mouseover', function() {
        tooltip.innerText = tooltipText;
        timer = setTimeout(function() {
          tooltip.setAttribute('style',
            'left:' + ((item.getBoundingClientRect().left + item.getBoundingClientRect().width) - ((item.getBoundingClientRect().width/2) + tooltip.getBoundingClientRect().width/2)) + 'px;' + 'top:' + (item.getBoundingClientRect().top - 25) + 'px;' + 'display: block; visibility: visible; opacity: 1;'
          );
          tooltip.classList.add(item.getAttribute('data-tooltip-style'));
        }, 400);
      });

      item.addEventListener('mouseout', function() {
        clearTimeout(timer);
        tooltip.setAttribute('style',
          'top: -900px; left: -900px; visibility: hidden; opacity: 0;'
        );
        tooltip.classList.remove(item.getAttribute('data-tooltip-style'));
      });
    });
  } catch (e) {
    console.log('jsModule: tooltip | ', e);
  }
};

export default tooltip;
