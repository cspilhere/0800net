import React from 'react'
import ReactDom from 'react-dom'
import { createHashHistory } from 'history'
import { Router, Route, Link, History, IndexRoute } from 'react-router'

import App from 'components/App';

import Welcome from 'components/ViewWelcome';
import Design from 'components/ViewDesign';
import Components from 'components/ViewComponents';


const domNode = document.querySelector('.sgb-wrapper');

const history = createHashHistory({
  queryKey: false
});

//https://github.com/rackt/react-router/tree/master/docs

ReactDom.render((
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={Welcome}/>
      <Route path="/welcome" component={Welcome} />
      <Route path="/design(/:section)" component={Design} />
      <Route path="/components(/:section)" component={Components} />
    </Route>
  </Router>
), domNode);
