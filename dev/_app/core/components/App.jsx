import React from 'react';
import {PropTypes} from 'react-router';

import Actions from '../actions/Actions';
import AppStore from '../stores/AppStore';

import Sidebar from './Sidebar';

import '../../theme/styles.scss';


export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sidebarNodes: {}
    };

    this._update = () => {
      this.setState({
        sidebarNodes: AppStore.getNodes(),
        welcome: AppStore.getWelcome()
      });
    }
  }

  componentWillMount() {
    AppStore.addChangeListener(this._update);
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._update);
  }

  componentDidMount() {
    Actions.fetchConfig();
  }

  render() {
    return (
      <div className="sgb-container">

        {/* .sgb-sidebar */}
        <Sidebar welcome={this.state.welcome} nodes={this.state.sidebarNodes} activePathName={this.props.location.pathname} />

        {/* Views */}
        {this.props.children}

      </div>
    );
  }
};

App.contextTypes = { history: PropTypes.history }
