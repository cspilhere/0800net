import React from 'react';

import AppStore from '../stores/AppStore';


export default class AppHeader extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      content: {}
    };

    this._update = () => {
      this.setState({
        content: AppStore.getWelcome(),
      });
    }
  }

  componentWillMount() {
    AppStore.addChangeListener(this._update);
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._update);
  }

  componentDidMount() {
    this.setState({
      content: AppStore.getWelcome(),
    });
  }

  render() {
    return (
      <footer className="sgb-main__footer" role="contentinfo">
        &copy; 2015 {this.state.content.name} - Todos os direitos reservados
      </footer>
    );
  }
};
