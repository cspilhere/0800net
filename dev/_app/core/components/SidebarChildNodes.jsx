import React from 'react';
import {Link} from 'react-router';

import classnames from 'classnames';

import {normalizeText} from 'utils/utils';


export default class SidebarChildNodes extends React.Component {
  constructor() {
    super();
  }

  render() {
    let childNodes = null;
    if(this.props.childNodes.length) {
      childNodes = this.props.childNodes.map((item, index) => {
        let regex = new RegExp(item + '\\b')
        let classes = classnames(
          'sgb-sidebar__childnodes-link',
          {'is-active': this.props.activePathName.match(regex)}
        );
        return (
          <li key={index} className="sgb-sidebar__childnodes-item">
            <Link className={classes} to={`/${this.props.parent}/${item}`}>{normalizeText(item)}</Link>
          </li>
        );
      });
    }
    return (
      <ul className="sgb-sidebar__childnodes">
        {childNodes}
      </ul>
    );
  }
};
