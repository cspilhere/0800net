import React from 'react';

import AppStore from '../stores/AppStore';

import ViewHeader from './ViewHeader';
import ViewFooter from './ViewFooter';


export default class ViewWelcome extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      content: {}
    };

    this._update = () => {
      this.setState({
        content: AppStore.getWelcome(),
      });
    }
  }

  componentWillMount() {
    AppStore.addChangeListener(this._update);
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._update);
  }

  componentDidMount() {
    this.setState({
      content: AppStore.getWelcome(),
    });
  }

  render() {
    return (
      <div className="sgb-main">
        <ViewHeader title={this.state.content.name} />
        <div className="sgb-main__view">
          <div className="sgb-main__view-description">
            <p className="sgb-body-text sgb-body-text--primary">{this.state.content.description}</p>
          </div>
          <div className="sgb-main__view-body">
            <a href="#" className="sgb-main_view-download-button">Faça o download dos estilos</a>
            <hr />
            <p className="sgb-main_view-vtag">Versão atual: {this.state.content.version}</p>
          </div>
        </div>
        <ViewFooter />
      </div>
    );
  }
};
