import {EventEmitter} from 'events';

import Dispatcher from '../dispatcher';
import ActionTypes from '../constants';
import CommonStore from './CommonStore';

import AppStore from './AppStore';


let _firstComponent = null;
let currentSection = null;

let _modules = [];


let ViewStore = Object.assign({}, EventEmitter.prototype, CommonStore, {

  getSectionInfo() {
    return require('dev/templates/' + currentSection + '/_data.json');
  },

  listJsModules() {
    // let modules = require.context('dev/scripts', true, /^((?![\\/]_app[\\/]).)*\.js$/);
    // let modulesList = [];
    // modules.keys().forEach((item, index) => {
    //   modulesList.push({
    //     'name': item.match(/[a-zA-Z-\/]+\.js/g)[0].match(/(?![.js\/])([a-zA-Z-\/]+)/g)[0],
    //     'path': item
    //   });
    // });
    return _modules;
  },

  getComponents() {
    let component = null;
    let data = {};
    let templates = [];
    let htmlString = null;
    let items = null;

    _modules = [];

    let moduleNames = [];

    component = currentSection;
    items = require('dev/templates/' + component + '/_data.json');
    items['items'].forEach((item) => {
      let currItem = item;
      let tmpl = item.template;

      if(currItem.jsModule) {
        if(moduleNames.indexOf(currItem.jsModule) === -1) {
          moduleNames.push(currItem.jsModule);
          _modules.push({
            name: currItem.jsModule,
            module: require('dev/scripts/' + currItem.jsModule)
          });
        }
      }

      try {
        htmlString = require('dev/templates/' + component + '/' + tmpl);
        templates.push({
          title: currItem.title,
          description: currItem.description,
          template: htmlString,
          jsModule: (currItem.jsModule) ? {name: currItem.jsModule, module: require('dev/scripts/' + currItem.jsModule)} : null
        });
      } catch (e) {
        templates.push(false);
        console.error(e);
        console.warn('That is an error on ' + component + '\'s _data.json file. Check if name, description and/or template field are configured correctly.');
      }
    });
    return templates;
  },

  componentSlug: function() {
    return currentSection;
  }

});


ViewStore.dispatchToken = Dispatcher.register(function(action) {
  switch(action.type) {
    case ActionTypes.FETCH_COMPONENTS:
      //Dispatcher.waitFor[AppStore.dispatchToken];
      setTimeout(() => {
        _firstComponent = AppStore.getComponents()[0];
        currentSection = action.section || _firstComponent;
        ViewStore.emitChange();
      }, 50);
      break;

    default:
  }
});

export default ViewStore;
