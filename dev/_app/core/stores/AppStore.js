import Dispatcher from '../dispatcher';
import ActionTypes from '../constants';
import CommonStore from './CommonStore';
import {EventEmitter} from 'events';


let _welcome = {};
let _design = {};
let _components = [];
let _nodes = {};
let _cwd = 'dev';


function populateData(content) {
  _welcome = content.welcome;
  _design = content.design;
  _nodes = content;
  _components = content.components;
}


let Store = Object.assign({}, EventEmitter.prototype, CommonStore, {

  getWelcome() {
    return _welcome;
  },

  getDesign() {
    return _design;
  },

  getComponents() {
    return _nodes.components;
  },

  getNodes() {
    _nodes.components = require.context('dev/templates/', true, /^\.\/.*\.json$/).keys();
    _nodes.components.map((item, index) => {
        _nodes.components[index] = item.match(/\.\/(?:(.*?))\/_data\.json/g) ?
          item.match(/\.\/(?:(.*?))\/_data\.json/g)[0]
          .replace(/\.\/(?:(.*?))\/_data\.json/g, '$1') : item;
      });
    return _nodes;
  }

});


Store.dispatchToken = Dispatcher.register(function(action) {
  switch(action.type) {
    case ActionTypes.FETCH_CONFIG:
      populateData(action.content);
      Store.emitChange();
      break;

    default:
  }
});

export default Store;
