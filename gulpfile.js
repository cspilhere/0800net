var gulp = require('gulp');
var run = require('run-sequence');

var exec = require('child_process').exec;

var connect = require('gulp-connect');

var ftp = require( 'vinyl-ftp' );
var guppy = require('git-guppy')(gulp);

var opn = require('opn');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var del = require('del');
var htmlreplace = require('gulp-html-replace');

var webpack = require('webpack');
var webpackStream = require('webpack-stream');

var webpackConfig = require('./webpack.config.js');
var webpackProdConfig = require('./webpack.production.config.js');

var pkg = require('./package.json');

// DEV: SERVER
gulp.task('webserver', function() {
  connect.server({
    root: ['./.tmp', './dev'],
    livereload: true,
    middleware: function(connect, opt) {
      return [];
    }
  });
  opn('http://localhost:8080');
});


// DEV: WATCH
gulp.task('webpackReload', function() { gulp.src(['./.tmp/scripts/bundle.js']).pipe(connect.reload()); });

gulp.task('watch', function () {
  gulp.watch(['dev/*.html'], ['html']);
  gulp.watch(['./.tmp/scripts/bundle.js'], ['webpackReload']);
  gulp.watch(['dev/styles/**/*.scss'], ['sass']);
});


// DEV: WEBPACK
gulp.task('webpack:dev', function(callback) {
  return gulp.src('./dev/_app/core/entry.js')
    .pipe(webpackStream(webpackConfig, webpack))
    .pipe(gulp.dest('./.tmp/'));
});


// DEV: HTML
gulp.task('html', function () {
  return gulp.src('./dev/*.html')
    .pipe(connect.reload());
});


// DEV:SASS
gulp.task('sass', function () {
  return gulp.src('./dev/styles/**/*.scss')
    .pipe(sass({ outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest('./.tmp/styles'))
    .pipe(connect.reload());
});


//DEV: CLEAN
gulp.task('clean', function (cb) {
  return del(['.tmp'], cb);
});



// BUILD /////////////////

// BUILD: WEBPACK
gulp.task('webpack:build', function(callback) {
  var devCompiler = webpack(Object.create(webpackProdConfig));
	devCompiler.run(function(err, stats) {
		if(err) throw new gutil.PluginError('webpack:build', err);
		gutil.log('[webpack:build]', stats.toString({
			colors: true
		}));
		callback();
	});
});

// BUILD: SASS
gulp.task('sass:build', function () {
  return gulp.src('./dev/styles/**/*.scss')
    .pipe(sass({ outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest('./dist/styles'))
    .pipe(connect.reload());
});

// BUILD: COPY
gulp.task('copy', function () {
	return gulp.src(['!dev/_app/**/*.*', '!dev/styles/**/*.*', 'dev/**/*.*'])
    .pipe(gulp.dest('./dist'));
});

//BUILD: HTML REPLACE TAGS
gulp.task('htmlReplace', function() {
  gulp.src('./dev/index.html')
    .pipe(htmlreplace({
        'js': './_app/bundle.min.js'
    }))
    .pipe(gulp.dest('./dist'));
});

//BUILD:CLEAN
gulp.task('clean:build', function (cb) {
  return del(['dist'], cb);
});


// TASKS ///////////

gulp.task('pre-push', guppy.src('pre-push', function (files, extra, cb) {
  exec('git rev-parse --abbrev-ref HEAD', function (err, stdout, stderr) {
    if (stdout.toString().split('\n')[0] === 'master') {
      console.log('o/');
//      run('build');
    } else {}
  });
}));
//
// gulp.task('ftp', ['htmlReplace'], function () {
//   var connect = ftp.create({
//     host: 'ftp.catarinasdesign.com.br',
//     user: 'catar782',
//     password: 'ptQ346G6pd',
//     log: gutil.log
//   });
//   var globs = [
//     'media/**/**',
//     '_app/**/**',
//     'styles/**/**',
//     'fonts/**',
//     'index.html'
//   ];
//   return gulp.src(globs, {
//     base: 'dist/',
//     cwd: 'dist/',
//     buffer: true
//   })
//   .pipe(connect.newer('/public_html/dev/' + pkg['ftpDir']))
//   .pipe(connect.dest('/public_html/dev/' + pkg['ftpDir']));
// });


//DEV: MAIN TASK
gulp.task('default', ['sass', 'webpack:dev', 'webserver', 'watch']);


//BUILD: MAIN TASK
gulp.task('build', function() {
  console.log('Building...');
  run('clean:build', 'copy', 'sass:build', 'webpack:build', 'htmlReplace', 'ftp');
});
