var path = require('path');
var webpack = require('webpack');

var node_modules = path.resolve('./', 'node_modules');

module.exports = {
	entry: {
    app: './dev/scripts/entry.js'
  },
	output: {
		path: path.join(__dirname, 'dist'),
    publicPath: 'dist/',
		filename: 'scripts/bundle.min.js',
    pathinfo: true
	},
  devtool: 'source-map',
  cache: true,
  debug: true,
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel?cacheDirectory'
      }
    ]
	},
	resolve: {
    unsafeCache: true,
    extensions: ['', '.js', '.jsx'],
		alias: {
			utils: path.resolve(__dirname, './dev/scripts/utils'),
		}
	},
	plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: {
        warnings: false
      }
    })
	]
};
